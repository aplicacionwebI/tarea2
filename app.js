const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input')

const expresiones = {
	nickname:  /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombres:  /^[a-zA-ZÀ-ḥ\s]{1,30}$/, // Letras y espacios, pueden llevar acentos.
    apellidos:  /^[a-zA-ZÀ-ḥ\s]{1,30}$/, // Letras y espacios, pueden llevar acentos.
	contraseña:  /^. {4,12}$/, // 4 a 12 digitos.
	correo:  /^[a-zA-Z0-9_.+-]+ @[a-zA-Z0-9-]+\. [a-zA-Z0-9-. ]+$/,
	telefono:  /^\d{7,14}$/  // 7 a 14 números.
}

const campos = {
	nickname: false,
	nombres: false,
    apellidos:false,
	contraseña: false,
	correo: false,
	telefono: false
}

const validarFormulario = (e) => {
    switch (e.target.name){
        case "nickname":
            case "nickname":
			validarCampo(expresiones.nickname, e.target, 'nickname');
		break;
		case "nombres":
			validarCampo(expresiones.nombres, e.target, 'nombres');
		break;
        case "apellidos":
			validarCampo(expresiones.apellidos, e.target, 'apellidos');
		break;
		case "contraseña":
			validarCampo(expresiones.contraseña, e.target, 'contraseña');
			validarContraseña2();
		break;
		case "contraseña2":
			validarContraseña2();
		break;
		case "correo":
			validarCampo(expresiones.correo, e.target, 'correo');
		break;
		case "telefono":
			validarCampo(expresiones.telefono, e.target, 'telefono');
		break;
    }
}

const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`${campo}`).classList.add('formulario__grupo-correcto');
		document.querySelector(`${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
	} else {
		document.getElementById(`${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`${campo}`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

const validarContraseña2 = () => {
	const inputPassword1 = document.getElementById('contraseña');
	const inputPassword2 = document.getElementById('Contraseña2');

	if(inputPassword1.value !== inputPassword2.value){
		document.getElementById(`contraseña2`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`contraseña2`).classList.remove('formulario__grupo-correcto');

		document.querySelector(`#contraseña2 .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos['contraseña'] = false;
	} else {
		document.getElementById(`contraseña2`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`contraseña2`).classList.add('formulario__grupo-correcto');
		
		document.querySelector(`#contraseña2 .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos['contraseña'] = true;
	}
}


inputs.forEach((input) => {
    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);
})

formulario.addEventListener('submit', (e) => {
    e.preventDefault();

    const terminos = document.getElementById('terminos');
	if(campos.usuario && campos.nombre && campos.password && campos.correo && campos.telefono && terminos.checked ){
		formulario.reset();

		document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		}, 5000);

		document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
			icono.classList.remove('formulario__grupo-correcto');
		});
	} else {
		document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
	}

})
